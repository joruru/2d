﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MovimientoPlayer : MonoBehaviour
{
    private int X;
    public bool PuedoSaltar;
    public Animator player;
    
    
    private float speed;
    public float jumpVelocity;
    public float fallMultiplier = 2.5f;
    public float lowJumpMultiplier = 2f;

    Rigidbody2D rb;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>(); 
    }


    // Start is called before the first frame update
    void Start()
    {
        X = 1;
        player = GetComponent<Animator>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if (Input.GetKey(KeyCode.Escape))
        {
            SceneManager.LoadScene("Menu", LoadSceneMode.Single);

        }


        speed = 2.5f;

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            speed = 7f;
            player.SetInteger("Condicion", 1);

        }

        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            player.SetInteger("Condicion", 0);
        }
        

        


        if (Input.GetKeyDown(KeyCode.Space) && PuedoSaltar == true)
        {
            GetComponent<Rigidbody2D>().velocity = Vector2.up * jumpVelocity;
            

        }
        


            if (rb.velocity.y < 0)
        {
            rb.velocity += Vector2.up * Physics.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
        }
        else if (rb.velocity.y > 0 && !Input.GetButton("Jump"))
        {
            rb.velocity += Vector2.up * Physics.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
        }

    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.transform.tag == "nofinal")
        {

            transform.Translate(new Vector3(-X, 0, 0) * Time.deltaTime * speed, Space.World);
            
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.tag == "nofinal")
        {
            player.SetInteger("Condicion", 3);
        }

        else if ((Input.GetKey(KeyCode.LeftArrow) && collision.transform.tag == "nofinal")){
            player.SetInteger("Condicion", 3);
        }
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "enemigo")
        {
            transform.position = new Vector3(612.22f, 1.96f, 0);
        }
    }


    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "ground")
        {
            PuedoSaltar = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "ground")
        {
            PuedoSaltar = false;
        }
    }

}

