﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimientoCamara : MonoBehaviour
{
    private int X;

    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        X = 1;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(new Vector3(X, 0, 0) * Time.deltaTime * speed, Space.World);
    }
}
